<?php

namespace Mbs\ControllerWithJavascript\ViewModel;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;

class UseMe implements ArgumentInterface
{
    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    public function __construct(
        UrlInterface $urlBuilder
    ) {
        $this->urlBuilder = $urlBuilder;
    }

    public function getMyUrl()
    {
        return $this->urlBuilder->getUrl('jsroute/test/myaction', ['myparam' => 'ItoldyouIdreachthecontroller']);
    }

}