define([
    "jquery"
], function ($) {
    'use strict';

    return function(config, element) {
        $("#js-trigger").on('click', function () {
            window.location = config.triggerUrl
        });
    }

});