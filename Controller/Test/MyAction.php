<?php

namespace Mbs\ControllerWithJavascript\Controller\Test;

use Magento\Framework\App\Action\HttpGetActionInterface;

class MyAction extends \Magento\Framework\App\Action\Action implements HttpGetActionInterface
{
    /**
     * @inheritDoc
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $getParams = $this->getRequest()->getParam('myparam');

        $this->getResponse()->setBody($getParams);
    }
}
