# README #

Create a minimalistic module that has a controller and a js that triggers the controller with a get parameter

### How do I get set up? ###

1. clone the repository
2. create folder app/code/Mbs/ControllerWithJavascript when located at the root of the Magento site
3. copy the content of this repository within the folder
4. install the module php bin/magento setup:upgrade

every page in the site should a button that tells 'launch controller'